Rails.application.routes.draw do
  devise_for :users
  resources :reservations
  get 'users/sign_in'
  root 'home#index' 
  resources :librarians
  resources :books
  resources :authors
  resources :categories
  resources :clients
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
