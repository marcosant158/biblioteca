# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
puts "Gerando as categorias(Categories)... "
20.times do |i|
Category.create!(
    name:Faker::Book.unique.genre
)
end
puts "Gerando as categorias(Categories)... [OK]"
puts "Gerando os Bibliotecários(Librarians)..."
3.times do |i|
Librarian.create!(
    email:Faker::Internet.unique.email,
)
end
puts "Gerando os Bibliotecários(Librarians)...[OK]"
puts "Gerando os nomes dos clientes(Clients)..."
10.times do |i|
Client.create!(
    name:Faker::Name.unique.name
)
end
puts "Gerando os nomes dos clientes(Clients)...[OK]"

puts "Gerando os Autores(Authors)..."
20.times do |i|
Author.create!(
    name:Faker::Book.unique.author
)
end
puts "Gerando os Autores(Authors)...[OK]"
puts "Gerando os Livros(Books)..."
20.times do |i|
Book.create!(
    name:Faker::Book.unique.title,
    author:Author.all.sample,
    category:Category.all.sample,
    stock:Faker::Number.decimal_part(digits: 2)
    )
end
puts "Gerando os Livros(Books)...[OK]"