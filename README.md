# README
Versões dos programas:
Ruby 2.5.1;
Rails 5.2.4;
Database PostgresSQL

Para popular o banco de dados (criará 20 categorias únicas, 3 bibliotecários, 10 clientes, 20 autores únicos e 20 livros):
rails db:seed 

Para cadastrar-se no sistema, use o botão cadastrar-se na tela inicial de login.

Para dar Log out, utilize o botão de seta que está presente na navbar.