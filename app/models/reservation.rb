class Reservation < ApplicationRecord
  belongs_to :book
  belongs_to :client
  belongs_to :librarian

  after_create :new_reservation

  after_destroy :destroy_reservation

  def new_reservation  
    self.book.update(stock: self.book.stock-1 )
  end
  def destroy_reservation
    self.book.update(stock: self.book.stock+1 )
  end 
end

